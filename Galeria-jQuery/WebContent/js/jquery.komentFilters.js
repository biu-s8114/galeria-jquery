(function( $ ) {
	$.fn.koment_check = function() {
		var slowa = [];
		$.ajax({
			  dataType: "json",
			  url: 'json/slowa.json',
			  async: false,
			  success: function(data){
				    for (var i in data){
				        slowa.push(data[i]);
				    }
			}});
		  $.each($(this).find("span"), function() {
			var text = '<div>Komentarz został ukryty. <button type="button" class="btn btn-danger" id="kom_pokaz">Pokaz</button></div>';
			var koment_t = $( this ).text();
			var koment_p = $( this ).parent();
			//is(slowa);
			for (i in slowa){
				//alert(slowa[i]);
				if (koment_t.contains(slowa[i]) && !(koment_p.next().has("button").length)){
						koment_p.hide().after(text);
						koment_p.next().find("#kom_pokaz").click( function (){					
							$(this).parent().remove();
                   			 koment_p.show();
					});
					};								
			}
		});
	};
}( jQuery ));