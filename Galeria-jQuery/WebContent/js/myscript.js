$(document).ready( function(){
	var k = [];
	$.getJSON('json/komentarze.json', function(data){
	    for (var i in data.komentarze){
	        k.push(data.komentarze[i]);
	    }
	    displayKoment($("#kom_zdj").val());
	});
	 function displayKoment(id){
	 	var x = 0;
	 	$("#lista_komentarzy").empty();
	 	 for(i in k){
	 		if (k[i]._id == id)
	 		{ 
	 			$("#lista_komentarzy").append('<div class="panel panel-default"><div class="panel-body"><strong>' + k[i]._autor + ': </strong><span id="kom_t">' + k[i]._tresc + '</span></div></div>' );		
				x++;
	}};
	if (x == 0) $("#lista_komentarzy").append('<div class="panel panel-default"><div class="panel-body">Brak komentarzy</div></div>' );	;
		
	$("#lista_komentarzy").koment_check();
	}	
	$( "#addKoment" ).click(function() {
			var id = $("#kom_zdj").val();
			var autor = $("#kom_autor").val();
			var tresc = $("#kom_tresc").val();
			if(autor.length > 0 && tresc.length > 0){
			k.push( {_id: id, _autor: autor, _tresc: tresc,});	
			$("#kom_autor").val("");
		    $("#kom_tresc").val("");
		    var jsonString = JSON.stringify(k);
		    saveJSON(jsonString);
		    displayKoment(id);
			}
			else{
				$("#addKomentForm").before('<div class="alert alert-danger">Nie wpisano poprawnie autora lub tresci komentarza.</div>');
				$(".alert").fadeOut(5000, function() {
					$("#kom_autor").focus();
				});
			}
	});
	$( "#miniaturki" ).click(function() {
		displayKoment($("#kom_zdj").val());
	});
	
	function saveJSON(komJSON){
		$(".panel-title").append('<img src="img/ajax_loader.gif" width="20" hight="20"/>')
		$.ajax({
			type: "POST",
			url: "komentarze.php",
			data: {'komentarze': komJSON},
			})
			.done(function() {
				$(".panel-title").find("img").delay(3000).slideUp(100);
				//alert( "Komentarze zapisano na serwerze!");
			});
	}
});
